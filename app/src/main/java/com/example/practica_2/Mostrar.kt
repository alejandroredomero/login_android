package com.example.practica_2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.practica_2.ui.theme.Practica_2Theme

class Mostrar : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Practica_2Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    EjercicioClase()
                }
            }
        }
    }
}


@Composable
fun EjercicioClase() {
    var num by remember {
        mutableStateOf(1)
    }
    var num2 by remember {
        mutableStateOf(0)
    }
    var id by remember {
        mutableStateOf(R.drawable.huerto_vacio)
    }

    Column(verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally) {
        Image(painter = painterResource(id = id),
            contentDescription = "huerto",
            modifier=Modifier.size(300.dp),
            contentScale= ContentScale.FillWidth
        )
        Button(
            onClick = {

                if (num ==1){
                    id= R.drawable.huerto_cosas
                    num = 2
                }else if (num == 2){
                    id = R.drawable.huerto_frutas
                    num = 3
                }else if (num == 3){
                    id = R.drawable.huerto_vacio
                    num = 1
                    num2 += (1..5).random()

                }
            }
        )
        {
            Text(text = "Presiona")
        }

        Text(text = "Precio = " + num2.toString())

    }
}




@Preview(showBackground = true)
@Composable
fun Huerto() {
    Practica_2Theme() {
        EjercicioClase()
    }
}


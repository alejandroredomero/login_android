package com.example.prueba

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.prueba.ui.theme.PruebaTheme
import org.w3c.dom.Text

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            PruebaTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    Huerto()

                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {

    var imagen = painterResource(id = R.drawable.mbappe)
    Image(painter = imagen,
        contentDescription = "mbappe",
        contentScale = ContentScale.Crop
    )

    Column() {
        Text(
        text = stringResource(id = R.string.saludo),
    )
        Row {


            Text(

                text = "1",
                modifier = modifier,
                color = Color.Red
            )
            Text(
                text = "2",
                modifier = modifier,
                Color.Blue
            )
            Text(
                text = "3",
                modifier = modifier,
                color = Color.Red
            )
        }

        Row() {
            Text(
                text = "4",
                modifier = modifier,
                Color.Blue
            )
            Text(
                text = "5",
                modifier = modifier,
                color = Color.Red
            )
            Text(
                text = "6",
                modifier = modifier,
                Color.Blue
            )
    }


    }


}

@Composable
fun EjercicioClase() {
    var num by remember {
        mutableStateOf(1)
    }
    var num2 by remember {
        mutableStateOf(0)
    }
    var id by remember {
        mutableStateOf(R.drawable.huerto_vacio)
    }

    Column(verticalArrangement = Arrangement.Center,
    horizontalAlignment = Alignment.CenterHorizontally) {
        Image(painter = painterResource(id = id),
            contentDescription = "huerto",
            modifier=Modifier.size(300.dp),
            contentScale=ContentScale.FillWidth
        )
        Button(
            onClick = {

                if (num ==1){
                    id= R.drawable.huerto_cosas
                    num = 2
                }else if (num == 2){
                    id = R.drawable.huerto_frutas
                    num = 3
                }else if (num == 3){
                    id = R.drawable.huerto_vacio
                    num = 1
                    num2 += (1..5).random()

                }
            }
        )
        {
            Text(text = "Presiona")
        }

        Text(text = "Precio = " + num2.toString())

    }
}




@Preview(showBackground = true)
@Composable
fun Huerto() {
    PruebaTheme {
        EjercicioClase()
    }





    @Composable
fun PruebaBotones(){
    var num by remember {
        mutableStateOf(1)
    }
    Column() {
        Button(
            onClick = {
                num = (1..10).random()

            }
        )
        {
            Text(text = "Boton")
        }
        Text(text = num.toString())
    }

}
@Composable
fun GreetingBotones() {
    PruebaTheme {
        PruebaBotones()
    }

}
    }

